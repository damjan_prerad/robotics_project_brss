# Robotics_project_BRAS

BRAS - Basic Robotic Algorithms Simulator

To run this robotics simulation you must download Processing from https://processing.org/

Done:
- Simulation of robot manipulator given the DH table
- Forward kinematics calculation
- Jacobian matrix calculation
- Implement manipulator end effector tracking
- Create a manipulator from a given DH table
- Implement forward kinematics algorithm
- Implement Jacobian transpose algorithm form inverse kinematics

Plan:

- Implement first person camera
- Implement inverse kinematics algorithm
- Interaction between objects to enable end effector to move objects in the workspace
- Simulation of robot manipulator in action (drawing or welding or sorting objects)
- Simple user interface
- Implement multiple inverse kinematics algorithms (Jacobian transpose, Pseudo inverse, Cyclic Coordinate Descent, Quasi-Newton, Conjugate gradient, ...)

Things to consider:
- Collision avoidance algorithm
- Multiple robot manipulators in a workspace
- Welding algorithm
- Pick and place robot
- Creation of 3D realistic robot arm parts
- Shadow implementation
- Using PMatrix class and its methods instead of float[][]

Here are a couple of screenshots of robix and scara manipulator from testing

![Scara manipulator from DH table](docu/Scara.png)
![Robix manipulator from DH table](docu/Robix.png)
![Mitsubishi manipulator from DH table](docu/Mitsubishi.png)
![Mitsubishi End Effector path highlighting](docu/Mitsubishi_end_effector_tracking.png)
![Mitsubishi manipulator following a straight line](docu/InverseKinematics_mitsubishi.png)
![Mitsubishi following a path given in worlds space coordinates, manual control](docu/InverseKinematics_mitsubishi_manual_control.png)
![Scara following rectangular path, manual commands given in world space cooridnates](docu/Scara_rectangle_path.png)