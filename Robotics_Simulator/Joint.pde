
//Consider renaming this class to Joint instead of a Segment

public class Joint{
  //Segment constrains must be added
  
  
 private float theta;        //Rotation around current z axis
 private float d;            //Translation around current z axis
 private float a;            //Translation around current x axis
 private float alpha;        //Rotation around current x axis
 private boolean jointType;  //0 for rotational joint, 1 for prismatic joint
 
 public Joint(float theta, float d, float a, float alpha, boolean jointType){
  this.theta = theta;
  this.d = d;
  this.a = a;
  this.alpha = alpha;
  this.jointType = jointType;
  
  println("Created a joint: " + theta + " " + d + " " + a + " " + alpha + " " + jointType);
 }
 
 public float getTheta(){
   return theta;
 }
 
 public float getD(){
   return d;
 }
 
 public float getA(){
   return a;
 }
 
 public float getAlpha(){
   return alpha;
 }
 
 public boolean getJointType(){
   return jointType;
 }
 
 public float getAction(){
  if(jointType) return d;
  else return theta;   
 }
 
 public float[][] getTransformationMatrix(){
  float[][] transformationMatrix = new float[4][4];
  
  float[][] zrotate = {
    {cos(theta), -sin(theta), 0, 0},
    {sin(theta),  cos(theta), 0, 0},
    {         0,           0, 1, 0},
    {         0,           0, 0, 1}
  };
  float[][] ztranslate = {
    {1, 0, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 1, d},
    {0, 0, 0, 1}
  };
  float[][] xtranslate = {
    {1, 0, 0, a},
    {0, 1, 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1}
  };
  float[][] xrotate = {
    {1,          0,           0, 0},
    {0, cos(alpha), -sin(alpha), 0},
    {0, sin(alpha),  cos(alpha), 0},
    {0,          0,           0, 1}
  };
  
  transformationMatrix = matrixMultiply(             zrotate, ztranslate);
  transformationMatrix = matrixMultiply(transformationMatrix, xtranslate);
  transformationMatrix = matrixMultiply(transformationMatrix, xrotate);
  return transformationMatrix;
 }
 
 public void jointAction(float action){
  //Action is either rotation in case of a rotational joint
  //or translational in case of a prismatic joint
  
  //if(jointType) d += action;
  //else theta += action;
  if(jointType) d = action;
  else theta = action;
 }
 
 public void drawJoint(){
   //if model is available then print the model and apply transformations
   //otherwise just print QUADS
  rotateZ(theta);
  
  beginShape(QUADS);
  vertex(-10,-10,0);
  vertex(10,-10,0);
  vertex(10,10,0);
  vertex(-10,10,0);
  
  vertex(-10,-10,0);
  vertex(10,-10,0);
  vertex(10,-10,d);
  vertex(-10,-10,d);
  
  vertex(10,-10,0);
  vertex(10,10,0);
  vertex(10,10,d);
  vertex(10,-10,d);
  
  vertex(10,10,0);
  vertex(-10,10,0);
  vertex(-10,10,d);
  vertex(10,10,d);
  
  vertex(-10,-10,0);
  vertex(-10,10,0);
  vertex(-10,10,d);
  vertex(-10,-10,d);
  
  vertex(-10,-10,d);
  vertex(10,-10,d);
  vertex(10,10,d);
  vertex(-10,10,d);
  endShape();
  
  translate(0,0,d);
    
  beginShape(QUADS);
  vertex(0,-10,-10);
  vertex(0,10,-10);
  vertex(0,10,10);
  vertex(0,-10,10);
  
  vertex(0,-10,-10);
  vertex(0,10,-10);
  vertex(a,10,-10);
  vertex(a,-10,-10);
  
  vertex(0,10,-10);
  vertex(0,10,10);
  vertex(a,10,10);
  vertex(a,10,-10);
  
  vertex(0,10,10);
  vertex(0,-10,10);
  vertex(a,-10,10);
  vertex(a,10,10);
  
  vertex(0,-10,-10);
  vertex(0,-10,10);
  vertex(a,-10,10);
  vertex(a,-10,-10);
  endShape();
  
  translate(a,0,0);
  
  beginShape(QUADS);
  vertex(0,-10,-10);
  vertex(0,10,-10);
  vertex(0,10,10);
  vertex(0,-10,10);
  endShape();
  
  rotateX(alpha);
 }
}
