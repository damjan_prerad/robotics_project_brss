

float[][] matrixMultiply(float[][] A, float[][] B){
 int rA = A.length;
 int cA = A[0].length;
 
 int rB = B.length;
 int cB = B[0].length;
 
 if(cA != rB){
  println("Matrix sizes are not compatible."); 
 }
 
 float[][] result = new float[rA][cB];
 
 for(int cr = 0; cr < rA; cr++){
   for(int cc = 0; cc < cB; cc++){
     float sum = 0;
     for(int i = 0; i < cA; i++){
       sum += A[cr][i] * B[i][cc];
     }
     result[cr][cc] = sum;
   }
 }
 return result;
}
