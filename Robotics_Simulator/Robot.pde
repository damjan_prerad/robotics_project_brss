

public class Robot{
 private float[][] dh_table;
 private int rows;
 
 private boolean enableTracking = false;
 
 private ArrayList<PVector> trackedTrajectory = new ArrayList<PVector>();
 
 private Joint[] robotJoints;
 
 private float translationViewCorrection;// Needed for correct view of tracking data (this is a workaround... )
 private float rotationViewCorrection;// Needed for correct view of tracking data (this is a workaround... )
 
 private void setTranslationAndRotationViewCorrection(float translationViewCorrection, float rotationViewCorrection){
  this.translationViewCorrection = translationViewCorrection; 
  this.rotationViewCorrection = rotationViewCorrection;
 }
 
 public Robot(float[][] dh_table){
   if(dh_table[0].length != 5){
     println("This is not a DH table!"); 
   }
   
   this.dh_table = dh_table;
   
   rows = dh_table.length;
   
   robotJoints = new Joint[rows];
   println("Robot creation in progess, rows value is " + rows);
   
   for(int i = 0; i < rows; i++){
     boolean hlp = false;
     if(dh_table[i][4] != 0) hlp = true;//check if it's a prismatic joint
    robotJoints[i] = new Joint(dh_table[i][0], dh_table[i][1], dh_table[i][2], dh_table[i][3], hlp); 
   }
 }
 
 public float[] getCartesianCoordinatesJacobianTranspose(
                       float[] start_joint, 
                       float[][] goal_cartesian){
    /*
     * This function aims to minimize the error_position to an acceptable value
     * error_position = goal - curent_position
     *
     * X = n(q) * q
     *
     * Algorithms in consideration for inverse kinematics:
     * - Cyclic Coordinate Descent
     * - Jacobian Transpose
     */
     
     float[] save = getJointSpacePosition();
     setJointSpacePosition(start_joint);
     float[][] pos_matrix = getCartesianSpacePosition();
     
     float alpha = 0.00001;
     
     float zrot_g = atan2(goal_cartesian[2][1], goal_cartesian[2][2]);
     float yrot_g = atan2(-sin(zrot_g)*goal_cartesian[2][0], goal_cartesian[2][1]);
     float xrot_g = atan2(goal_cartesian[1][0], goal_cartesian[0][0]);
 
     float zrot_p = atan2(pos_matrix[2][1], pos_matrix[2][2]);
     float yrot_p = atan2(-sin(zrot_p)*pos_matrix[2][0], pos_matrix[2][1]);
     float xrot_p = atan2(pos_matrix[1][0], pos_matrix[0][0]);
     
     float[] goal_position = { goal_cartesian[0][3], goal_cartesian[1][3], goal_cartesian[2][3], xrot_g, yrot_g, zrot_g};
     float[] start_position = { pos_matrix[0][3], pos_matrix[1][3], pos_matrix[2][3], xrot_p, yrot_p, zrot_p};
     
     float[] error = subtractVectors(goal_position, start_position);
     
     float[] ne = new float[6];//6 because three cooridantes for position and three coordinats for rotation
     for(int i = 0; i < error.length; i++){
      ne[i] = error[i];
     }
     
     float[] new_position = { start_position[0], start_position[1], start_position[2], xrot_p, yrot_p, zrot_p};
     
     float[] new_joints = new float[start_joint.length];
     for(int i = 0; i < start_joint.length; i++){
      new_joints[i] = start_joint[i]; 
     }

     int count = 0;
     while(distance(ne) > 0.01){
       count ++;
       if(count > 2000) break; //If the algoritham is not done after 2000 iterations, break from loop
       
       setJointSpacePosition(new_joints);
       pos_matrix = getCartesianSpacePosition();
       new_position[0] = pos_matrix[0][3];
       new_position[1] = pos_matrix[1][3];
       new_position[2] = pos_matrix[2][3];
       
       zrot_p = atan2(pos_matrix[2][1], pos_matrix[2][2]);
       yrot_p = atan2(-sin(zrot_p)*pos_matrix[2][0], pos_matrix[2][1]);
       xrot_p = atan2(pos_matrix[1][0], pos_matrix[0][0]);
       
       new_position[3] = xrot_p;
       new_position[4] = yrot_p;
       new_position[5] = zrot_p;
       
       float[][] J = getJacobianMatrix();
       float[][] JT = transpose(J);
       
       error = subtractVectors(goal_position, new_position);
       for(int i = 0; i < error.length; i++){
        ne[i] = error[i];
       }

       float[] dq = multiplyVectorWithScalar(multiplyMatrixWithVector(JT, ne), alpha);
       new_joints = addVectors(dq, new_joints);
     }
     setJointSpacePosition(save);
     return new_joints;
 }
 
 private float[] createRotationalJacobianColumn(int joint){
   float[] column = new float[6];
   
   float[][] t_matrix_n = getCartesianSpacePosition();
   
   if(joint == 0) joint = -1;
   float[][] t_matrix_i = getJointCartesianSpacePosition(joint);
   
   float[] R0i = {t_matrix_i[0][2], t_matrix_i[1][2], t_matrix_i[2][2]};
   float[] dn = {t_matrix_n[0][3], t_matrix_n[1][3], t_matrix_n[2][3]};
   float[] di = {t_matrix_i[0][3], t_matrix_i[1][3], t_matrix_i[2][3]};
   float[] linear_part = crossProduct3D(R0i, subtract3D(dn, di));
   
   column[0] = linear_part[0];
   column[1] = linear_part[1];
   column[2] = linear_part[2];
   column[3] = R0i[0];
   column[4] = R0i[1];
   column[5] = R0i[2];
   
   return column;
 }
 
 private float[] createPrismaticJacobianColumn(int joint){
   float[] column = new float[6];
   
   if(joint == 0) joint = -1;
   float[][] t_matrix_i = getJointCartesianSpacePosition(joint);
   
   float[] R0i = {t_matrix_i[0][2], t_matrix_i[1][2], t_matrix_i[2][2]};
   
   column[0] = R0i[0];
   column[1] = R0i[1];
   column[2] = R0i[2];
   column[3] = 0;
   column[4] = 0;
   column[5] = 0;
   
   return column;
 }
 
 private float[] createJacobianColumn(int joint){
   if(robotJoints[joint].getJointType() == false){
     return createRotationalJacobianColumn(joint); 
   }
   else{
     return createPrismaticJacobianColumn(joint);
   }
 }
 
 public float[][] getJacobianMatrix(){
  return getJacobianMatrixAtPosition(getJointSpacePosition()); 
 }
 
 public void trackEndEffector(boolean enableTracking){
   this.enableTracking = enableTracking;
 }
 
 public void clearTrackingData(){
  trackedTrajectory.clear(); 
 }
 
 public float[][] getJacobianMatrixAtPosition(float[] jointSpacePosition){
   //X = f(Q) -> X = F * Q
   // dX/dt = dF/dQ * dQ/dt //partial differentiation
   // J = dF/dQ
   // dX = J * dQ/dt
   // J is a Jacobian matrix
   
   float[] save = getJointSpacePosition();
   setJointSpacePosition(jointSpacePosition);
   
   float[][] jacobian = new float[6][rows];

   float[] column = new float[6];

   for(int i = 0; i < rows; i++){
    column = createJacobianColumn(i);
    
    jacobian[0][i] = column[0];
    jacobian[1][i] = column[1]; 
    jacobian[2][i] = column[2]; 
    jacobian[3][i] = column[3]; 
    jacobian[4][i] = column[4]; 
    jacobian[5][i] = column[5]; 
   }
   
   setJointSpacePosition(save);
   return jacobian;
 }
 
 public float[][] getCartesianSpacePosition(){
   return getJointCartesianSpacePosition(rows);
 }
 
 public float[][] getJointCartesianSpacePosition(int jointNumber){
     if(jointNumber > rows){
      println("Nonexistant segment number"); 
     }
     
     float[][] result = {
      {1, 0, 0, 0},
      {0, 1, 0, 0},
      {0, 0, 1, 0},
      {0, 0, 0, 1},
     };
     if(jointNumber == -1) return result;
     
     result = robotJoints[0].getTransformationMatrix();
     
     for(int i = 1; i < jointNumber; i++){
       result = matrixMultiply(result, robotJoints[i].getTransformationMatrix()); 
     }
     
     return result;
 }
 
 public float[] getJointSpacePosition(){
   //This returns angles in radians
   float[] jointSpace = new float[rows];
   for(int i = 0; i < rows; i++){
    jointSpace[i] = robotJoints[i].getAction(); 
   }
   return jointSpace;
 }
 
 public void setJointSpacePosition(float[] q){
  if(q.length != rows){
    println("q size does not match the number of joints this robot has"); 
  }
  else{
    for(int i = 0; i < rows; i++)
      robotJoints[i].jointAction(q[i]);
  }
 }
  
 float[] prevp = {0, 0, 0};
  
 public void drawRobot(){
   
   if(enableTracking){
     fill(255,255,255);
     for( PVector i: trackedTrajectory){
      pushMatrix();
      //resetMatrix();
      rotateX(-rotationViewCorrection);
      translate(-width/2,-height/2,translationViewCorrection);
      translate(i.x, i.y, i.z);
      box(2);
      popMatrix();
     }
   }
   
   fill(128,62,86);//Robot color
   stroke(128,128,128);
   for(int i = 0; i < rows; i++){
    robotJoints[i].drawJoint(); 
   }
   
   if(enableTracking){
     float x, y, z;
     x = modelX(0, 0, 0);
     y = modelY(0, 0, 0);
     z = modelZ(0, 0, 0);
     if(abs(prevp[0] - x) > 0.001 || abs(prevp[0] - z) > 0.001 || abs(prevp[0] - z) > 0.001){
       trackedTrajectory.add(new PVector(x, y, z));
       prevp[0] = x;
       prevp[1] = y;
       prevp[2] = z;
     }
   }
 }
}
