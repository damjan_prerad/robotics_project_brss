
float[][] robix_dh = {
  {0,     0,  95.0,     PI, 0},
  {0,     0,  90.0, PI / 2, 0},
  {0, -28.0, 150.0,      0, 0}
};

float[][] scara_dh = {
 {0,   87, -30,  0, 0}, 
 {0,     0, -70,  0, 0}, 
 {0,  -15, -40, PI, 0}, 
 {0,   20,    0,  0, 1}
};

float[][] mitsubishi_dh = {
 {   0, 295,   0, PI/2, 0},
 {   PI/2,   0, 230,    0, 0},
 {   0,   0,  50,-PI/2, 0},
 {   0, 270,   0, PI/2, 0},
 {   0,   0,   0,-PI/2, 0},
 {   0,  70,   0,    0, 0}
};

Robot robix = new Robot(robix_dh);
Robot scara = new Robot(scara_dh);
Robot mitsubishi = new Robot(mitsubishi_dh);

float[][] current;

void setup(){
  size(800,800,P3D);
}

float rotation = 0;

float q1 = 0;
float q2 = 0;
float q3 = 0;
float q4 = 0;
float q5 = 0;
float q6 = 0;

boolean enbl = false;
float translation = 50;

int robotSelector = 0;
int switchControl = 0;

void jointUpdate(){
 if(keyPressed){
  if(key == 'q') q1+=0.01;
  if(key == 'a') q1-=0.01;
  if(key == 'w') q2+=0.01;
  if(key == 's') q2-=0.01;
  if(key == 'e') q3+=0.01;
  if(key == 'd') q3-=0.01;
  if(key == 'r') q4+=0.01;
  if(key == 'f') q4-=0.01;
  if(key == 't') q5+=0.01;
  if(key == 'g') q5-=0.01;
  if(key == 'y') q6+=0.01;
  if(key == 'h') q6-=0.01;
  
  if(key == '[') rotation+=0.01;
  if(key == '\'') rotation-=0.01;
  
  if(key == 'c'){
    switch(robotSelector){
    case 1://Mitsubishi
      println("Jacobian for Mitsubishi manipulator is:");
      printMatrix(mitsubishi.getJacobianMatrix());
    break;
    case 2://Jacobian
       println("Jacobian for SCARA manipulator is:");
       printMatrix(scara.getJacobianMatrix());
    break;
    case 3://Robix
      println("Jacobian for Robix manipulator is:");
      printMatrix(robix.getJacobianMatrix());
    break;
    }
  }
  
  if(key == 'x'){
   switch(robotSelector){
    case 1:
    mitsubishi.trackEndEffector(enbl);
    break;
    
    case 2:
    case 4:
    scara.trackEndEffector(enbl);
    break;
    
    case 3:
    robix.trackEndEffector(enbl);
    break;
   }

   
   enbl = !enbl;
  }
  if(key == 'v'){
    switch(robotSelector){
    case 1:
    mitsubishi.clearTrackingData();
    break;
    
    case 2:
    case 4:
    scara.clearTrackingData(); 
    break;
    
    case 3:
    robix.clearTrackingData();
    break;
   }  
  }
  if(key == ']'){
    q1=0;
    q2=0;
    q3=0;
    q4=0;
    q5=0;
    q6=0;
   switchControl = 0; 
  }
  if(key == '\\'){
    q1=0;
    q2=0;
    q3=0;
    q4=0;
    q5=0;
    q6=0;
   switchControl = 1; 
  }
 }
 if(key == '1'){
   robotSelector = 1;
 }
 
 if(key == '2'){
   robotSelector = 2;
 }
 
 if(key == '3'){
   robotSelector = 3;
 }
 
 if(key == '4'){
   robotSelector = 4; 
 }
}

void draw(){
   background(82,62,62);
   stroke(128,128,128);
   
   ambientLight(128,128,128);
   pointLight(212,84,32,0,0,0);
   
   mitsubishi.setTranslationAndRotationViewCorrection(translation,rotation);
   scara.setTranslationAndRotationViewCorrection(translation,rotation);
   robix.setTranslationAndRotationViewCorrection(translation,rotation);
   translate(width/2,height/2,-translation);
   rotateX(rotation);
   
   //Draw worksurface
   fill(32,64,128);
   pushMatrix();
   translate(0,0,-10);
   box(400,400,10);
   popMatrix();
   
   jointUpdate();
   
   float[] q;
   
   float[][] next_pos;
   float  current_pos[];
   float[] cr;
   
   switch(robotSelector){
    case 1://Mitsubishi
    
    q = new float[6];
    q[0] = q1;
    q[1] = q2;
    q[2] = q3;
    q[3] = q4;
    q[4] = q5;
    q[5] = q6;
    
    if(switchControl == 1){
      next_pos = mitsubishi.getCartesianSpacePosition();
      next_pos[0][3] = 50 - 200*q1;
      next_pos[1][3] = -50 - 200*q2;
      next_pos[2][3] = 50 - 200*q3;
      current_pos = mitsubishi.getJointSpacePosition();
      cr = mitsubishi.getCartesianCoordinatesJacobianTranspose(current_pos, next_pos);
      mitsubishi.setJointSpacePosition(cr);
    }
    else{
     mitsubishi.setJointSpacePosition(q); 
    }
    mitsubishi.drawRobot();
    
    break;
    
    case 2://SCARA
    
    q = new float[4];
    q[0] = q1;
    q[1] = q2;
    q[2] = q3;
    q[3] = 100*q4;
    
    if(switchControl == 1){
      next_pos = scara.getCartesianSpacePosition();
      next_pos[0][3] = 50 - 200*q1;
      next_pos[1][3] = -50 - 200*q2;
      next_pos[2][3] = 50 - 200*q3;
      current_pos = scara.getJointSpacePosition();
      cr = scara.getCartesianCoordinatesJacobianTranspose(current_pos, next_pos);
      scara.setJointSpacePosition(cr);
    }
    else{
     scara.setJointSpacePosition(q); 
    }
    scara.drawRobot();
    
    break;
    
    case 3://Robix
    
    q = new float[3];
    q[0] = q1;
    q[1] = q2;
    q[2] = q3;
    
    if(switchControl == 1){
      next_pos = robix.getCartesianSpacePosition();
      next_pos[0][3] = 50 - 200*q1;
      next_pos[1][3] = -50 - 200*q2;
      next_pos[2][3] = 50 - 200*q3;
      
      current_pos = robix.getJointSpacePosition();
      cr = robix.getCartesianCoordinatesJacobianTranspose(current_pos, next_pos);
      
      robix.setJointSpacePosition(cr);
    }
    else{
     robix.setJointSpacePosition(q); 
    }
    robix.drawRobot();
    
    break;
    
    case 4:
        
        float[] point = getPoint();
        
        next_pos = scara.getCartesianSpacePosition();
        next_pos[0][3] = point[0];
        next_pos[1][3] = point[1];
        next_pos[2][3] = point[2];
        current_pos = scara.getJointSpacePosition();
        cr = scara.getCartesianCoordinatesJacobianTranspose(current_pos, next_pos);
        scara.setJointSpacePosition(cr);

      scara.drawRobot();
    break;
   }
}

int counter = 0;
int speed = 0;
float[] getPoint(){
  if(counter == 83) {
    counter = -1;
    scara.clearTrackingData(); 
  }
  speed++;
  if(speed == 2){
  counter ++;
  speed = 0;
  }
  
  if(counter == 0)
  return new float[]{-100, 50, 0};
  if(counter == 1)
  return new float[]{-95, 50, 0};
  if(counter == 2)
  return new float[]{-90, 60, 0};
  if(counter == 3)
  return new float[]{-87.5, 70, 0};
  if(counter == 4)
  return new float[]{-87.5, 80, 0};
  if(counter == 5)
  return new float[]{-90, 90, 0};
  if(counter == 6)
  return new float[]{-95, 100, 0};
  if(counter == 7)
  return new float[]{-100, 100, 0};
  if(counter == 8)
  return new float[]{-100, 90, 0};
  if(counter == 9)
  return new float[]{-100, 80, 0};
  if(counter == 10)
  return new float[]{-100, 70, 0};
  if(counter == 11)
  return new float[]{-100, 60, 0};
  
  if(counter == 12)
  return new float[]{-80, 100, 0};
  if(counter == 13)
  return new float[]{-80, 90, 0};
  if(counter == 14)
  return new float[]{-77.5, 80, 0};
  if(counter == 15)
  return new float[]{-75, 70, 0};
  if(counter == 16)
  return new float[]{-72.5, 60, 0};
  if(counter == 17)
  return new float[]{-70, 50, 0};
  if(counter == 18)
  return new float[]{-67.5, 60, 0};
  if(counter == 19)
  return new float[]{-65, 70, 0};
  if(counter == 20)
  return new float[]{-62.5, 80, 0};
  if(counter == 21)
  return new float[]{-60, 90, 0};
  if(counter == 22)
  return new float[]{-60, 100, 0};
  if(counter == 23)
  return new float[]{-65, 80, 0};
  if(counter == 24)
  return new float[]{-70, 80, 0};
  if(counter == 25)
  return new float[]{-75, 80, 0};
  
  if(counter == 26)
  return new float[]{-50, 100, 0};
  if(counter == 27)
  return new float[]{-50, 90, 0};
  if(counter == 28)
  return new float[]{-50, 80, 0};
  if(counter == 29)
  return new float[]{-50, 70, 0};
  if(counter == 30)
  return new float[]{-50, 60, 0};
  if(counter == 31)
  return new float[]{-50, 50, 0};
  if(counter == 32)
  return new float[]{-45, 60, 0};
  if(counter == 33)
  return new float[]{-40, 70, 0};
  if(counter == 34)
  return new float[]{-37.5, 80, 0};
  if(counter == 35)
  return new float[]{-35, 70, 0};
  if(counter == 36)
  return new float[]{-30, 60, 0};
  if(counter == 37)
  return new float[]{-25, 50, 0};
  if(counter == 38)
  return new float[]{-25, 60, 0};
  if(counter == 39)
  return new float[]{-25, 70, 0};
  if(counter == 40)
  return new float[]{-25, 80, 0};
  if(counter == 41)
  return new float[]{-25, 90, 0};
  if(counter == 42)
  return new float[]{-25, 100, 0};
  
  if(counter == 43)
  return new float[]{-15, 80, 0};
  if(counter == 44)
  return new float[]{-12.5, 90, 0};
  if(counter == 45)
  return new float[]{-10, 100, 0};
  if(counter == 46)
  return new float[]{-5, 100, 0};
  if(counter == 47)
  return new float[]{-2.5, 90, 0};
  if(counter == 48)
  return new float[]{0, 80, 0};
  if(counter == 49)
  return new float[]{0, 70, 0};
  if(counter == 50)
  return new float[]{0, 60, 0};
  if(counter == 51)
  return new float[]{0, 50, 0};
  if(counter == 52)
  return new float[]{-5, 50, 0};
  if(counter == 53)
  return new float[]{-10, 50, 0};

  if(counter == 54)
  return new float[]{5, 100, 0};
  if(counter == 55)
  return new float[]{5, 90, 0};
  if(counter == 56)
  return new float[]{7.5, 80, 0};
  if(counter == 57)
  return new float[]{10, 70, 0};
  if(counter == 58)
  return new float[]{12.5, 60, 0};
  if(counter == 59)
  return new float[]{15, 50, 0};
  if(counter == 60)
  return new float[]{17.5, 60, 0};
  if(counter == 61)
  return new float[]{20, 70, 0};
  if(counter == 62)
  return new float[]{22.5, 80, 0};
  if(counter == 63)
  return new float[]{25, 90, 0};
  if(counter == 64)
  return new float[]{25, 100, 0};
  if(counter == 65)
  return new float[]{20, 80, 0};
  if(counter == 66)
  return new float[]{15, 80, 0};
  if(counter == 67)
  return new float[]{10, 80, 0};

  if(counter == 68)
  return new float[]{30, 100, 0};
  if(counter == 69)
  return new float[]{30, 90, 0};
  if(counter == 70)
  return new float[]{30, 80, 0};
  if(counter == 71)
  return new float[]{30, 70, 0};
  if(counter == 72)
  return new float[]{30, 60, 0};
  if(counter == 73)
  return new float[]{30, 50, 0};
  if(counter == 74)
  return new float[]{32.5, 60, 0};
  if(counter == 75)
  return new float[]{35, 70, 0};
  if(counter == 76)
  return new float[]{37.5, 80, 0};
  if(counter == 77)
  return new float[]{40, 90, 0};
  if(counter == 78)
  return new float[]{42.5, 100, 0};
  if(counter == 79)
  return new float[]{42.5, 90, 0};
  if(counter == 80)
  return new float[]{42.5, 80, 0};
  if(counter == 81)
  return new float[]{42.5, 70, 0};
  if(counter == 82)
  return new float[]{42.5, 60, 0};
  if(counter == 83)
  return new float[]{42.5, 50, 0};

  return new float[]{0, 0, 0};
}
