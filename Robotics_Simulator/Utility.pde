

void printMatrix(float[][] matrix){
  int rows = matrix.length;
  int columns = matrix[0].length;
  
  for(int i = 0; i < rows; i++){
   for(int j = 0; j < columns; j++){
     print(matrix[i][j]+ " ");
   }
   println();
  }
}

public float[] crossProduct3D(float[] A, float[] B){
 if(A.length != B.length){
   println("Error in function crossProduct3D, dimensions do not match");
 }
 float[] result = new float[3];
 result[0] = A[1] * B[2] - A[2] * B[1];
 result[1] = A[2] * B[0] - A[0] * B[2];
 result[2] = A[0] * B[1] - A[1] * B[0];
 return result;
}

public float[] subtractVectors(float[] A, float[] B){
  float[] result = new float[A.length];
  for(int i = 0; i < A.length; i++){
   result[i] = A[i] - B[i]; 
  }
  return result;
}

public float[] addVectors(float[] A, float[] B){
  if(A.length != B.length){
   println("Error in function addVectors, dimensions do not match");
  }
  float[] result = new float[A.length];
  for(int i = 0; i < A.length; i++){
   result[i] = A[i] + B[i]; 
  }
  return result;
}

public float[] subtract3D(float[] A, float[] B){
  //return subtract(A, B); // Use this instead of the lines below
  //Not changed because the function subtract was not tested
 if(A.length != B.length){
   println("Error in function subtract 3D, dimensions do not match");
 }
 float[] result = new float[3];
 result[0] = A[0] - B[0];
 result[1] = A[1] - B[1];
 result[2] = A[2] - B[2];
 return result;
}

public float distance(float[] A){
  //return distance(A, new float[3]);
 float sum = 0;
 for(int i = 0; i < A.length; i++){
  sum += A[i] * A[i]; 
 }
 sum = sqrt(sum);
 return sum;
}

public float distance(float[] A, float[] B){
  if(A.length != B.length){
   println("Error in function distance, dimensions do not match");
  }
  float sum = 0;
  for(int i = 0; i < A.length; i++){
    sum += (A[i] - B[i]) * (A[i] - B[i]);
  }
  return sqrt(sum);
}

public float[][] transpose(float[][] A){
  float[][] result = new float[A[0].length][A.length];
  
  for(int i = 0; i < A.length; i++){
    for(int j = 0; j < A[0].length; j++){
        result[j][i] = A[i][j];
    }
  }
 return result;
}

public float[] multiplyMatrixWithVector(float[][] A, float[] B){
  if(A[0].length != B.length){
    println("Error in function multiplyMatrixWithVector"); 
  }
  float[] result = new float[A.length];
  for(int i = 0; i < A.length; i++){
    result[i] = 0;
    for(int j = 0; j < A[0].length; j ++){
      result[i] += A[i][j] * B[j];
    }
  }
  return result;
}

public float[] multiplyVectorWithScalar(float[] A, float b){
 float[] result = new float[A.length];
 for(int i = 0; i < A.length; i++){
  result[i] = b * A[i]; 
 }
 return result;
}

public float[][] subtractMatrix(float[][] A, float[][] B){
  if(A.length != B.length || A[0].length != B[0].length){
   println("Error in function subtract matrix"); 
  }
  
  float[][] result = new float[A.length][A[0].length];
  for(int i = 0; i < A.length; i++){
   for(int j = 0; j< A[0].length; j++){
     result[i][j] = A[i][j] - B[i][j];
   }
  }
  return result;
}
